
var popup = document.getElementById("popup");
var selectionStat = document.getElementById("selectioninfo");
var summary = {};

(function(){ 

var dsv = d3.dsv(";", "text/plain");

var retentionContainerID = 'dashboard';

var retentionContainer = $('#'+retentionContainerID);

	var data = retention_data;

	var maxDays = d3.max(data, function(d){ return d.days_since_last_order});
		maxDays = Math.ceil(maxDays/10)*10 + 10;
	var maxOrders = d3.max(data, function(d){ return d.num_orders});
		maxOrders++;
	var maxTotals = d3.max(data, function(d){ return d.total_sales});

	var groups = getKeys(data.map(function(d){ return d.group_name }));
	var color = d3.scale.category10().domain(groups);

console.log(groups);

	var margin = {top: 50, right: 200, bottom: 30, left: 50},
		width = 900,
		height = 500;

  	var format = d3.format("d");
  	var format2 = d3.format(".2s");

	var x = d3.scale.linear()                           // set x-scale 
      .domain([0, maxDays])
      .range([width, 0]);

  	var xAxis = d3.svg.axis()                           
      .scale(x)
      .orient("bottom");

    var y = d3.scale.linear()                           // set y-scale 
      .domain([0, maxOrders])
      .range([height, 0]);

  	var yAxis = d3.svg.axis()                           
      .scale(y)
      .tickFormat(format)
      .orient("left");

    var z = d3.scale.linear()                           // set scale for stars
      .domain([0, maxTotals])
      .range([1, 5]);

	var svg = d3.select("#"+retentionContainerID).append("svg");

	svg = svg.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom)
		//.style("margin-left", margin.left + "px")
		.on("mousedown", mousedown)
		.on("mousemove", mousemove)
		.on("mouseup", mouseup)
		.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	svg.append("g")
		.attr("class", "x axis")
		.attr("transform", "translate(0," + height + ")")
		.call(xAxis)
	  .append("text")
	    .attr("x", width)
	    .attr("y", -16)
	    .attr("dy", ".71em")
	    .style("text-anchor", "end")
	    .text("Days after last order");

	svg.append("g")
		.attr("class", "y axis")
		.call(yAxis)
	  .append("text")
	    .attr("transform", "rotate(-90)")
	    .attr("y", 6)
	    .attr("dy", ".71em")
	    .style("text-anchor", "end")
	    .text("Number of Orders");

      var points = svg.selectAll(".points")
          .data(data);

      var starPath ="M 0.000 1.000 L 3.000 5.196 L 0.866 0.500 L 6.000 0.000 L 0.866 -0.500 L 3.000 -5.196 L 0.000 -1.000 L -3.000 -5.196 L -0.866 -0.500 L -6.000 0.000 L -0.866 0.500 L -3.000 5.196 z";

      points.enter().append("g")
          .attr("transform", function(d){ return "translate("+x(d.days_since_last_order)+"," + y(d.num_orders) + ")"})
      	.insert("path")
          .attr("class", "points")
          .attr("d", starPath)
          .attr("transform", function(d){ return "scale("+z(d.total_sales)+"," + z(d.total_sales) + ")"})
        .on("mouseenter", function(d){ showTooltip(d); })
        .on("mouseout", function(d){ hideTooltip(); })          
          .style("fill", function(d){ return color(d.group_name) });  


  var legend = svg.selectAll(".legend")  // create a legend
      .data(groups)               // data is an array of Continents
    .enter().append("g")
      .attr("class", "legend")
      .attr("transform", function(d, i) { return "translate("+(width)+"," + (i+1) * 28 + ")"; });

  legendRect = legend.append("rect")
      .attr("class", "selectable")
      .attr("x", 70)
      .attr("width", 22)
      .attr("height", 22)
      .on("click", toggleKey )
      .style("fill", color);

  legend.append("text")
      .attr("x", 100)
      .attr("y", 9)
      .attr("dy", ".35em")
      .text(function(d) { return d; });

  	var coord, rect, dragging;

	function mousedown(d){

		var offset = retentionContainer.offset();

		d3.event.preventDefault();

		if (d3.event.target.nodeName === 'rect') return;

		if (!dragging) {

			points.classed('selected', false);
			legendRect.classed('selected', false);
			d3.select('#'+retentionContainerID).classed('deselected', true);

			coord = {};
			dragging = true;
			coord.x0 = d3.event.pageX - margin.left - offset.left;
			coord.y0 = d3.event.pageY - margin.top - offset.top;
			rect = svg.append('rect').classed('selectionrect', true);
		}
	}
	function mousemove(e){
		if (!dragging) return;

		var offset = retentionContainer.offset();

		coord.w = d3.event.pageX - margin.left - offset.left - coord.x0;
		coord.h = d3.event.pageY - margin.top - offset.top - coord.y0;

		var x0 = (coord.w < 0) ? coord.x0 + coord.w : coord.x0,
			x1 = x0 + Math.abs(coord.w),
			y0 = (coord.h < 0) ? coord.y0 + coord.h : coord.y0,
			y1 = y0 + Math.abs(coord.h);

		rect.attr({
			"x": x0,
			"y": y0,
			"width": Math.abs(coord.w),
			"height": Math.abs(coord.h)
		});

		points.classed('selected', function(d){

			var xx = x(d.days_since_last_order), yy = y(d.num_orders);

			return ( (x0 < xx && xx < x1) && (y0 < yy && yy < y1 ) );

		});

	}	
	function mouseup(e){ 
		
		dragging = false;
		if (rect) rect.remove();

		if (d3.event.target.nodeName === 'rect' && $(d3.event.target).hasClass('selectable')) return;



		if ( !d3.select('.selected')[0][0] ) {

			closeSelectionStat();
			d3.select('#'+retentionContainerID).classed('deselected', false);

		} else { 
			openSelectionStat();
		}
	}
	function toggleKey(d){

		points.classed('selected', false);
		legendRect.classed('selected', false);
		d3.select('#'+retentionContainerID).classed('deselected', true);

		d3.select(this).classed('selected', true);

		points.classed('selected', function(dd){

			return d === dd.group_name;
		});

		openSelectionStat();
	}

	function openSelectionStat(){ 

		summary.total = 0, summary.customers = 0, summary.num_orders = 0;
		summary.items = [];

		points.filter('.selected').each(function(d, idx){

			summary.total += d.total_sales;
			summary.num_orders += d.num_orders;
			summary.customers++;
			summary.items.push(d);
		});

	  	var html = '<tr><td>Customers : </td><td>'+summary.customers+
	            '</td></tr><tr><td>Orders : </td><td>'+summary.num_orders+
	            '</td></tr><tr><td>Totals : </td><td>'+ '$' +format2(summary.total)+
	            '</td></tr>';

	  	selectionStat.firstElementChild.innerHTML = html;		

		var offset = retentionContainer.offset();

		selectionStat.style.display = 'block';
		selectionStat.style.top = offset.top + 400 + 'px';
		selectionStat.style.left = offset.left + 1020 + 'px';
	}
	function closeSelectionStat(){

		selectionStat.style.display = 'none';
	}

})();

function showTooltip(data){    // mouseenter event handler

	var html;
	if(data.cust_name)
  		html = '<tr><td>Customer name : </td><td>'+data.cust_name+
            '</td></tr><tr><td>email : </td><td>'+data.email+
            '</td></tr><tr><td>totals : </td><td>'+ '$' +data.total_sales+
            '</td></tr>';
    else html = '<tr><td>Number of Customers : </td><td>'+data.num_customers+
            '</td></tr><tr><td>Sales : </td><td>'+'$'+data.sales+
            '</td></tr>';

  popup.firstElementChild.innerHTML = html;

  moveTooltip();
  popup.style.display = 'block';            // show pop-up

}

function hideTooltip(data){       // mouseleave event handler
  popup.style.display = 'none';   // hide pop-up
}

function moveTooltip(d) {

  var x = d3.event.pageX + 10,
      y = d3.event.pageY,
      tooltipHeight = popup.clientHeight;
      if (x > 800) x -= 260;
      if (y > 500 - tooltipHeight) y -= tooltipHeight+10;

    d3.select(popup).style({ 'top' : y+'px', 'left': x+'px' });
}

function getKeys(data){   // reate a list of data[0] fields
	var keys = []; 
  	data.forEach(function(d) {
    	
    		var j = 0;
    		while ( j < keys.length && keys[j] != d){j++;}
    		if ( j == keys.length) keys.push(d);
    	
  	});
  	return keys;
}

function savedata(){
	
	console.log('save', summary.items);

	var str = ConvertToCSV(summary.items);

	var blob = new Blob([str], {type: "text/plain"});
    saveAs(blob, "customer_list.csv");
}

function ConvertToCSV(objArray) {
    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    var str = '';
	var line = '';

    for (index in array[0]){
    	if (line != '') line += ','
    	line += index;
    }
	str += line + '\r\n';

    for (var i = 0; i < array.length; i++) {
        line = '';
        for ( index in array[i]) {
            if (line != '') line += ','

            line += array[i][index];
        }

        str += line + '\r\n';
    }
    str += '\r\n';

    return str;
}
