
(function(){
	console.log(cohort_data);

	var format = d3.format("02");
 	var data = cohort_data;

 	var maxSales = 0, maxCustomers = 0;

 	data.forEach(function(d){
 		d.y = d.start_month + '/' + d.start_year;
 		d.x = d.order_month - d.start_month + (d.order_year - d.start_year)*12;
 		if (maxCustomers < d.num_customers) maxCustomers = d.num_customers;
 		if (maxSales < d.sales) maxSales = d.sales;
 	});

 	data.sort(function(a,b){
 		return (a.start_year*12 + a.start_month) - (b.start_year*12 + b.start_month);
 	});

	console.log('data',data);
 	//start_year, start_month, order_year, order_month, num_customers, sales

 	var cohortContainerID = 'cohorttable';

 	var salesmode = true;

	var margin = {top: 50, right: 200, bottom: 30, left: 100},
		width = 900,
		height = 500;

	var x = d3.scale.ordinal()                           // set x-scale 
      .domain(getKeys(data.map(function(d){ return d.x; })).sort(function(a,b){ return a - b}))
      .rangeRoundBands([0, width], .1);

  	var xAxis = d3.svg.axis()                           
      .scale(x)
      .orient("top");

    var y = d3.scale.ordinal()                           // set y-scale 
      .domain(getKeys(data.map(function(d){ return d.y; })))
      .rangeRoundBands([0, height], .1);

  	var yAxis = d3.svg.axis()                           
      .scale(y)
      .orient("left");
//["#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd", "#8c564b", "#e377c2", "#7f7f7f", "#bcbd22", "#17becf"] 
	var color_a = d3.interpolateRgb( d3.rgb('#FFFFFF'),  d3.rgb("#1f77b4"));
	var color_b = d3.interpolateRgb( d3.rgb('#FFFFFF'),  d3.rgb("#9467bd"));
	
	var color1 = d3.scale.linear()
		.range([0, 1])
		.domain([0, maxSales]);

	var color2 = d3.scale.linear()
		.range([0, 1])
		.domain([0, maxCustomers]);

	var svg = d3.select("#"+cohortContainerID).append("svg");

	svg = svg.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom)
		.append("g")
		.on('click', toggleMode)
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	svg.append("g")
		.attr("class", "x axis")
		.call(xAxis)
	  .append("text")
	    .attr("x", width/2)
	    .attr("y", -36)
	    .attr("dy", ".71em")
	    .style("text-anchor", "end")
	    .text("Months after Start Date");

	svg.append("g")
		.attr("class", "y axis")
		.call(yAxis)
	  .append("text")
	    .attr("transform", "rotate(-90)")
	    .attr("x", -26)
	    .attr("y", -86)
	    .attr("dy", ".71em")
	    .style("text-anchor", "end")
	    .text("Start Date");

	  var key = svg.selectAll(".cohortcell")
	      .data(data)
	    .enter().append("g")
	      .attr("class", 'cohortcell')
	      .on('mouseenter', toggleView)
	      .on('mouseleave', restoreView)
	      .attr("transform", function(d) { return "translate(" + x(d.x) + ","+ y(d.y)+")"; });

	  key.append("rect")
	      .attr("width", x.rangeBand())
	      .attr("x", 0)
	      .attr("y", 0)
	      .attr("height", y.rangeBand());

	  var rect = key.append("rect")
	  	  .attr("class", 'rectmask')
	  	  .attr("fill", function(d){ return color_a( color1(d.sales) ) })
	      .attr("width", x.rangeBand())
	      .attr("x", 0)
	      .attr("y", 0)
	      .attr("height", y.rangeBand());

	  var txt = key.append("text")
	      .attr("class", "keytext")
	      .attr("transform", function(d) { return "translate(" + (7) + "," + (y.rangeBand()/2+2) + ")";	})
	      .text(function(d) { return (salesmode) ? ('$'+d.sales) : d. num_customers})
		  .style("fill", "black");

    function toggleView(d){

    	showTooltip(d);

    	//$(this).find('.rectmask').fadeTo(200, 0.4);
    	d3.select(this).transition().duration(300).attr('opacity', 0.6);

    	/*
    	var txt = $(this).find('text');
    	
    	txt.hide();
    	//txt.text((!salesmode) ? ('$'+d.sales) : d. num_customers);
    	txt.show(400); */
    }
    function restoreView(d){

    	hideTooltip(d);

    	//$(this).find('.rectmask').fadeTo(300, 1);
    	d3.select(this).transition().duration(400).attr('opacity', 1);
    	/*
    	var txt = $(this).find('text');
    	
    	txt.hide();
    	txt.text((salesmode) ? ('$'+d.sales) : d. num_customers);
    	txt.show(400); */
    }
    function toggleMode(d){
    	salesmode = !salesmode;
    	txt.text(function(d) { return (salesmode) ? ('$'+d.sales) : d. num_customers});
    	rect.attr("fill", function(d){ return (salesmode) ? color_a( color1(d.sales) ) : color_b( color2(d.num_customers) ) })

    	$('#experiments h2').text((salesmode) ? 'Sales' : 'Number of Customers');
    }
})();