from flask import Flask, request, session, g, redirect, render_template, Response
import csv
import simplejson
import numpy as np
import psycopg2
import psycopg2.extras
from sklearn.cluster import KMeans
import tempfile
import uuid

app = Flask(__name__)
app.secret_key = '123'
app.debug = True

@app.teardown_appcontext
def close_db(error):
	if hasattr(g, 'db'):
		g.db.close()

@app.before_request
def before_req():
#	g.db = psycopg2.connect("dbname='heroku' user='root' host='localhost' password='root'", cursor_factory=psycopg2.extras.RealDictCursor)
#	g.db = psycopg2.connect("postgres://pziegpomkaraqc:t1FCIzV7l2TUiPJRLjtqdYSsIg@ec2-54-243-245-58.compute-1.amazonaws.com:5432/dc91gc79c5lqgb", cursor_factory=psycopg2.extras.RealDictCursor)
	g.db = psycopg2.connect("postgres://wxwaxgezoakgim:x1gHesF_9ZlciBUNukisc2DmAD@ec2-54-225-101-191.compute-1.amazonaws.com:5432/d6425sgnp4i852", cursor_factory=psycopg2.extras.RealDictCursor)

@app.route('/')
def main():
	return render_template('main.html')

@app.route('/upload', methods=['POST'])
def upload():
	f = request.files['file']
	data = f.read()
	fd, filename = tempfile.mkstemp()
	file(filename, 'w').write(data)
	cur = g.db.cursor()
	cur.autocommit = False
	report_uuid = uuid.uuid4().hex
	cur.execute('insert into reports(created_at, uuid) values (localtimestamp, %s)', (report_uuid,))
	cur.execute("select currval('reports_id_seq')")
	row = cur.fetchone()
	report_id = row['currval']

	csvfile = open(filename)
	reader = csv.DictReader(csvfile)
	data = []
	for row in reader:
		data.append(row)

	for d in data:
		sql = 'insert into data_table (report_id, name, shipping_name, email, created_at, total) values(%s, %s, %s, %s, %s, %s)'
		cur.execute(sql, [report_id, d['Name'], d['Shipping_Name'], d['Email'], d['Created_at'] or None, d['Total'] or None])
	g.db.commit()
	return redirect('/reports/%s'%report_uuid)

@app.route('/reports')
def reports():
	cur = g.db.cursor()
	cur.execute('select * from reports order by id desc')
	reports = cur.fetchall()
	return render_template('reports.html', reports=reports)

@app.route('/reports/<string:report_uuid>')
def report(report_uuid):
	cur = g.db.cursor()
	cur.execute('select id from reports where uuid=%s', (report_uuid,))
	row = cur.fetchone()
	report_id = row['id']
	cur.execute("""begin; select cohort_data('x', %s); fetch all in "x";"""%report_id)
	g.db.commit()
	cohort_data = cur.fetchall()
	cur.execute("""begin; select retention_data('x', %s); fetch all in "x";"""%report_id)
	g.db.commit()
	retention_data = cur.fetchall()

	km = KMeans(n_clusters=7)#, random_state=1)
	kdata = [(row['num_orders'], row['days_since_last_order']) for row in retention_data]
	groups = km.fit_predict(kdata)
	group_names = ['New Customers', 'Sleepers', 'Drifting', 'Loyal Customers', 'Has Potential', 'At Risk', 'Red Alert']
	for i in range(len(retention_data)):
		retention_data[i]['group'] = groups[i] + 1
		retention_data[i]['group_name'] = group_names[groups[i]]
		retention_data[i]['last_purchase_dt'] = retention_data[i]['last_purchase_dt'].isoformat()
	cohort_json = simplejson.dumps(cohort_data, indent=2)
	retention_json = simplejson.dumps(retention_data, indent=2)
	return render_template('report.html', cohort_data=cohort_data, retention_data=retention_data, cohort_json=cohort_json, retention_json=retention_json)

if __name__ == "__main__":
	app.debug = True
	app.run(host='0.0.0.0')
