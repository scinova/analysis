drop table if exists data_table;
drop table if exists reports;

create table reports (
	id serial primary key,
	uuid varchar,
	created_at timestamp
	);

create table data_table (
	report_id integer references reports(id),
	name varchar not null,
	shipping_name varchar,
	email varchar not null,
	created_at timestamp,
	total integer
	);

drop function if exists retention_data(ref refcursor, id integer);
create function retention_data(ref refcursor, id integer) returns refcursor as $$
	begin
	create temp table ret_grid_step1 as
		select
			name as order_name,
			shipping_name as cust_name,
			email,
			date(created_at) as created_at,
			count(*) as num_items_in_order,
			sum(total) as sales
		from data_table
		where report_id=id and created_at is not null
		group by 1,2,3,4;
	open ref for
		select
			b.cust_name,
			a.email,
			a.last_purchase_dt,
			total_sales,
			num_orders,
			current_date - last_purchase_dt as days_since_last_order
		from (
			select
				email,
				max(created_at) as last_purchase_dt,
				count(*) as num_orders,
				sum(sales) as total_sales
			from ret_grid_step1
			group by 1
		) as a
		left join (
			select
				email,
				cust_name,
				rank() over (partition BY email ORDER BY created_at DESC) as rnk
			from ret_grid_step1
			group by 1,2,created_at
		) as b
		on a.email = b.email
		where b.rnk = 1;
	return ref;
	end;
$$ language plpgsql;

drop function if exists cohort_data(ref refcursor, id integer);
create function cohort_data(ref refcursor, id integer) returns refcursor as $$
	begin

	create temp table cohort_step1 as
		select
			email,
			date(created_at) as created_at,
			sum(total) as sales
			from data_table
			where report_id=id and created_at is not null
			group by 1,2;
	create temp table cohort_step2 as
		select
			email,
			min(created_at) as start_dt,
			sum(sales) as total_sales
		from cohort_step1
		group by 1;
	create temp table cohort_step3 as
		select
			a.email,
			a.start_dt,
			extract(week from a.start_dt) as start_week,
			extract(month from a.start_dt) as start_month,
			extract(year from a.start_dt) as start_year,
			b.created_at as order_dt,
			extract(week from b.created_at) as order_week,
			extract(month from b.created_at) as order_month,
			extract(year from b.created_at) as order_year,
			sum(b.sales) as sales
		from cohort_step2 as a
		left join cohort_step1 as b
		on a.email = b.email
		group by 1,2,3,4,5,6,7,8,9;
	open ref for
		select
			start_year,
			start_month,
			order_year,
			order_month,
			sum(sales) as sales,
			count(*) as num_customers
		from cohort_step3
		group by 1,2,3,4
		order by start_year, start_month, order_year, order_month;
	return ref;
	end;
$$ language plpgsql;
